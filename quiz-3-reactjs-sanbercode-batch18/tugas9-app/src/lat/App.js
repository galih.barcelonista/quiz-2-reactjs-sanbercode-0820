import React from 'react';
import logo from './logo.svg';
import './App.css';

class Welcome extends React.Component{
  render(){
    const cardName ={
      border: "1px solid",
      borderRadius: "20px",
      width: "25%",
      margin: "0 auto",
      padding: "50px",
      textAlign: "center",
      marginTop: "20px",
      color: this.props.color === undefined ? "black" : this.props.color

    }
  return <h2 style={cardName}>Helloo, {this.props.name}</h2>;
  }
}
function App() {
  var manusia = [
    {name : "Gabriel",age:24, color:"blue"},
    {name : "Claudia",age:21, color:"pink"},
    {name : "Angela",age:20},
    {name : "Samuel",age:23,color:"purple"}
  ]
  return (
        <div>
          {
           manusia.map((item)=>{
             return(
               <Welcome name={item.name} color={item.color} />
             )
           }

           )

          }
          
        </div>
  );
}


export default App;
