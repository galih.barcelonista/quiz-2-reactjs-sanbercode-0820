import React, { useContext,useEffect } from "react"
import {MoviesContext} from './MoviesContext'
import axios from "axios"

const MoviesList = () =>{
    const [movies,setMovies] = useContext(MoviesContext)
    useEffect( () => {
        if (movies.lists === null){
          axios.get(`http://backendexample.sanbercloud.com/api/movies`)
          .then(res => {
            setMovies({
              ...movies, 
              lists: res.data.map(el=>{ 
                return {id: el.id,
                    title: el.title,
                    duration: el.duration,
                    description: el.description, 
                    year: el.year,
                    genre: el.genre,
                    rating: el.rating,
                    img_url: el.img_url
                }
              })
            })
          })
        }
      }, [setMovies, movies])
    
      const handleEdit = (event) =>{
        let idDataMovies = parseInt(event.target.value)
        setMovies({...movies, selectedId: idDataMovies, statusForm: "changeToEdit"})
      }
    
      const handleDelete = (event) => {
        let idDataMovies = parseInt(event.target.value)
    
        let newLists = movies.lists.filter(el => el.id !== idDataMovies)
    
        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idDataMovies}`)
        .then(res => {
          console.log(res)
        })
              
        setMovies({...movies, lists: [...newLists]})
        
      }
    

    return(
        <>
             <h1>Daftar Film</h1>
          <table>
            <thead>
              <tr>
                <th>No</th>
                <th>Title</th>
                <th>Description</th>
                <th>Year</th>
                <th>Duration</th>
                <th>Genre</th>
                <th>Rating</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                {
                    movies.lists !== null && (
                  movies.lists.map((item,index) => {
                    return(                    
                      <tr key={index}>
                        <td>{index+1}</td>
                        <td>{item.title}</td>
                        <td>{item.description}</td>
                        <td>{item.year}</td>
                        <td>{item.duration}</td>
                        <td>{item.genre}</td>
                        <td>{item.rating}</td>
                        <td>
                          <button onClick={handleEdit} value={item.id}>Edit</button>
                          &nbsp;
                          <button onClick={handleDelete} value={item.id}>Delete</button>
                        </td>
                      </tr>
                    )
                  })
                    )}
            </tbody>
          </table>

        </>
    )

}



export default MoviesList