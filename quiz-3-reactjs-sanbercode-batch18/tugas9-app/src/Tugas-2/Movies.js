import React from "react"
import {MoviesProvider} from "./MoviesContext"
import MoviesList from "./MoviesList"
import MoviesForm from "./MoviesForm"


const Movies = ()=>{

    return(
        <div>
        <MoviesProvider>
        <MoviesList/>
          <MoviesForm/>
        </MoviesProvider>
        </div>
    )
}


export default Movies