import React, {useContext, useState, useEffect} from "react"
import {MoviesContext} from './MoviesContext'
import axios from "axios"

const MoviesForm = () =>{
    const [movies,setMovies] = useContext(MoviesContext)
    const [input, setInput] = useState({title:"",
    description:"",year:"2020",duration:"120",genre:"",rating:0,img_url:"",id:null})

    useEffect(()=>{
        if (movies.statusForm === "changeToEdit"){
          let dataMovies = movies.lists.find(x=> x.id === movies.selectedId)
          setInput({title: dataMovies.title, 
            description: dataMovies.description, 
            duration: dataMovies.duration,
            year: dataMovies.year,
            genre: dataMovies.genre,
            rating: dataMovies.rating,
            img_url: dataMovies.img_url
        })
          setMovies({...movies, statusForm: "edit"})
        }
      },[movies, setMovies])
    

      const handleChange = (event) =>{
        let typeOfInput = event.target.name
    
        switch (typeOfInput){
          case "title":
          {
            setInput({...input, title: event.target.value});
            break
          }
          case "description":
          {
            setInput({...input, description: event.target.value});
            break
          }
          case "duration":
            {
              setInput({...input, duration: event.target.value});
              break
            }
          case "year":
          {
            setInput({...input, year: event.target.value});
              break
          }
          case "genre":
            {
              setInput({...input, genre: event.target.value});
                break
            }
            case "rating":
                {
                  setInput({...input, rating: event.target.value});
                    break
                }
            case "img_url":
                {
                    setInput({...input, img_url: event.target.value});
                    break
                }
    
        default:
          {break;}
        }
      }
      
      const handleSubmit = (event) =>{
        // menahan submit
        event.preventDefault()
    
        let title = input.title
        let description = input.description
        let duration = input.duration
        let year = input.year
        let genre = input.genre
        let rating = input.rating
        let img_url = input.img_url
        
    
        if (movies.statusForm === "create"){        
          axios.post(`http://backendexample.sanbercloud.com/api/movies`, {title,description,duration,year,genre,rating,img_url})
          .then(res => {
              setMovies(
                {statusForm: "create", selectedId: 0,
                lists: [
                  ...movies.lists, 
                  { id: res.data.id, 
                    title: input.title,
                    description: input.description,
                    duration: input.duration,
                    year: input.year,
                    genre: input.genre,
                    rating: input.rating,
                    img_url: input.img_url
                    
                  }]
                })
          })
        }else if(movies.statusForm === "edit"){
          axios.put(`http://backendexample.sanbercloud.com/api/movies/${movies.selectedId}`, {title,description,duration,year,genre,rating,img_url})
          .then(() => {
              let dataMovies = movies.lists.find(el=> el.id === movies.selectedId)
              dataMovies.title = input.title
              dataMovies.description = input.description
              dataMovies.duration = input.duration
              dataMovies.year = input.year
              dataMovies.genre = input.genre
              dataMovies.rating = input.rating
              dataMovies.img_url = input.img_url
              setMovies({statusForm: "create", selectedId: 0, lists: [...movies.lists]})
          })
        }
    
        setInput({title:"",description:"",year:"2020",duration:"120",genre:"",rating:0,img_url:""})
    
      }
    
  
  return(
      <>
       <h1>Movies Form</h1>
          <div style={{width: "50%", margin: "0 auto", display: "block"}}>
            <div style={{border: "1px solid #aaa", padding: "20px"}}>
              <form onSubmit={handleSubmit}>
                <label style={{float: "left"}}>
                  Title:
                </label>
                <input style={{float: "right"}} type="text" required name="title" value={input.title} onChange={handleChange}/>
                <br/>
                <br/>
                <label style={{float: "left"}}>
                  Description:
                </label>
                <input style={{float: "right"}} type="text" required name="description" value={input.description} onChange={handleChange}/>
                <br/>
                <br/>
                <label style={{float: "left"}}>
                  Year:
                </label>
                <input style={{float: "right"}} type="year" required name="year" value={input.year} onChange={handleChange}/>
                <br/>
                <label style={{float: "left"}}>
                  Duration:
                </label>
                <input style={{float: "right"}} type="text" required name="duration" value={input.duration} onChange={handleChange}/>
                <br/>
                <label style={{float: "left"}}>
                  Genre:
                </label>
                <input style={{float: "right"}} type="text" required name="genre" value={input.genre} onChange={handleChange}/>
                <br/>
                <label style={{float: "left"}}>
                  Rating:
                </label>
                <input style={{float: "right"}} type="text" required name="rating" value={input.rating} onChange={handleChange}/>
                <br/>
                <label style={{float: "left"}}>
                  Img URL:
                </label>
                <input style={{float: "right"}} type="text" required name="img_url" value={input.img_url} onChange={handleChange}/>
                <br/>
                <br/>
                <div style={{width: "100%", paddingBottom: "20px"}}>
                  <button style={{ float: "right"}}>submit</button>
                </div>
              </form>
            </div>
          </div>


      </>
  )
}


export default MoviesForm