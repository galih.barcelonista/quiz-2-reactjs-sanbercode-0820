import React from 'react';


class UserInfo extends React.Component{
  render(){
    return(
      <div>
        <p>{this.props.nama} </p>
        <p>{this.props.email} </p>
        <p>{this.props.so} </p>
        <p>{this.props.gitlab} </p>
        <p>{this.props.tele} </p>
      </div>
    )
  }
}
const data = [{nama:"Adelia",email:"adelia06@gmail.com",so:"Linux",gitlab:"adelia06",tele:"adelia06"}]
class About extends React.Component{
  
  render(){

    return(
      <body>
      <div>
        <h1>Data Peserta Sanbercode Bootcamp Reactjs</h1>
        { data.map((item)=>{
          return(
       
        <ol>
          <li><strong >Nama:</strong> {item.nama}</li> 
          <li><strong >Email:</strong> {item.email}</li> 
          <li><strong >Sistem Operasi yang digunakan:</strong> {item.so}</li>
          <li><strong >Akun Gitlab:</strong> {item.gitlab}</li> 
          <li><strong >Akun Telegram:</strong> {item.tele}</li> 
        </ol>
          )
    }) }
      </div>
      <br/>
      <br/>
    </body>
    )

  }

}
export default About