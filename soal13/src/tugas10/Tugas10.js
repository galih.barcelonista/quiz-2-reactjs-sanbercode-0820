import React from 'react';

class Nama extends React.Component{
    
    render(){
    return <>{this.props.name}</>;
    }
}

class Usia extends React.Component{
    
    render(){
    return <p>{this.props.age} years old</p>;
    }
}

class Gender extends React.Component{
    
    render(){
    return <p>{this.props.gender}</p>;
    }
}

class Profesi extends React.Component{
    
    render(){
    return <p>{this.props.profession}</p>;
    }
}



const data = [{name: "John", age: 25, gender: "Male", profession: "Engineer", 
photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"}, 
{name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"}, 
{name: "David", age: 30, gender: "Male", profession: "Programmer", photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"}, 
{name: "Kate", age: 27, gender: "Female", profession: "Model", 
photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }]
class Tugas10 extends React.Component{
    render(){

        const cardName ={
            border: "1px solid",
            borderRadius: "20px",
            width: "25%",
            margin: "0 auto",
            padding: "50px",
            textAlign: "center",
            marginTop: "20px",
          }
      
        return(
        <div>
            {
                data.map((item)=>{
                    return(
                        <div style={cardName}>
                        <>
                            <img src={item.photo} style={{width:"350px",height:"350px"}} ></img><br/>
                            <h2><Nama name={item.name}/></h2><br />
                           <h2><Profesi profession={item.profession}/></h2><br />
                           <h2><Usia age={item.age}/></h2><br />
                        </>
                        </div>
                    )
                }

                )
            }
        </div>
        )
    }
}

export default Tugas10