
class BangunDatar{
    constructor(nama){
        this.bangunNama = nama;
    }

    luas(){
        return "";
    }

    keliling(){
        return "";
    }

    get cnam(){
        return this.bangunNama;
    }

    set cnam(x){
        this.bangunNama=x;
    }
}

// myBangun = new BangunDatar("Lingkaran");
// console.log(myBangun.cnam)

class Lingkaran extends BangunDatar{
    constructor(nama,r){
        super(nama);
        this.jari=r;
    }

    luas(){
        return 3.14*this.jari;
    }

    keliling(){
        return 2*3.14*this.jari;
    }

    get cnam(){
        return this.jari;
    }

    set cnam(x){
        this.jari=x;
    }
}

class Persegi extends BangunDatar{
    constructor(nama,s){
        super(nama);
        this.sisi=s;
    }

    luas(){
        return this.sisi*this.sisi;
    }

    keliling(){
        return 4*this.sisi;
    }

    get cnam(){
        return this.sisi;
    }

    set cnam(x){
        this.sisi=x;
    }
}


